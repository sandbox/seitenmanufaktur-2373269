<?php

/**
 * @file media_podlove/themes/media_podlove.theme.inc
 *
 * Theme and preprocess functions for Media: Podlove.
 */

/**
 * Preprocess function for theme('media_podlove_audio').
 */
function media_podlove_preprocess_media_podlove_audio(&$variables) {
  drupal_add_css(libraries_get_path('podlove-web-player') . '/podlove-web-player/static/podlove-web-player.css');
  drupal_add_js(libraries_get_path('podlove-web-player') . '/podlove-web-player/libs/html5shiv.js');
  // drupal_add_js(libraries_get_path('podlove-web-player') . '/podlove-web-player/libs/jquery-1.9.1.min.js');
  drupal_add_js(libraries_get_path('podlove-web-player') . '/podlove-web-player/static/podlove-web-player.js');

  $file = $variables['file'];
  $ref_entity = $variables['referencing_entity'];
  $settings = $variables['options'];
  $settings_with_tokens = _media_podlove_settings_with_tokens();
  $settings_defaults = _media_podlove_settings_defaults();

  $token_data = array('file' => $file, 'node' => $ref_entity);

  $variables['playersettings'] = array();

  foreach($settings as $setting_key => $setting_value) {
    if (!empty($setting_value)) {
      if (in_array($setting_key, $settings_with_tokens)) {
        $variables['playersettings'][$setting_key] = token_replace($setting_value, $token_data);
      } else if (is_bool($settings_defaults[$setting_key])) {
        $variables['playersettings'][$setting_key] = ($setting_value === TRUE) ? TRUE : FALSE;
      } else {
        $variables['playersettings'][$setting_key] = $setting_value;
      }
    }
  }

  $poster = token_replace($settings['source_poster'], $token_data);

  if (is_string($poster)) {
    $variables['poster'] = $poster;
  }

  $variables['playersettings_json'] = json_encode($variables['playersettings']);

  dsm($variables);
}
