<?php

/**
 * @file media_podlove/themes/media-podlove-audio.tpl.php
 *
 * Template file for theme('media_podlove_audio').
 *
 * Variables available:
 *  $options - An array containing the Media Youtube formatter options.
 */
?>
<?php if (!empty($sources)) : ?>
  <audio id="podloveplayer<?php print $id; ?>" class="<?php print $classes; ?>">
    <?php foreach ($sources as $source): ?>
      <source src="<?php echo $source['url']; ?>" type="<?php echo $source['mimetype']; ?>"></source>
      <!--
      <source src="samples/podlove-test-track.ogg" type="audio/ogg; codecs=vorbis"></source>
      <source src="samples/podlove-test-track.opus" type="audio/ogg; codecs=opus"></source>
      -->
    <?php endforeach; ?>
  </audio>
<?php endif; ?>

<script>
  jQuery('#podloveplayer<?php print $id; ?>').podlovewebplayer(<?php print $playersettings_json; ?>);
</script>
