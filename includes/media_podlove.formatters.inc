<?php

/**
 * implements hook_file_formatter_info()
 */
function media_podlove_file_formatter_info() {
  $formatters['podlove_player'] = array(
    'label' => t('Podlove Player'),
    'file types' => array('audio'),
    'default settings' => array(),
    'view callback' => 'media_podlove_file_formatter_audio_view',
    'settings callback' => 'media_podlove_file_formatter_audio_settings',
    'mime types' => array('audio/*'),
  );

  $formatters['podlove_player']['default settings'] = _media_podlove_settings_defaults();

  return $formatters;
}

/**
 * View callback for podlove_player file formatter
 */
function media_podlove_file_formatter_audio_view($file, $display, $langcode) {
  // WYSIWYG does not yet support video inside a running editor instance.
  if (empty($file->override['wysiwyg'])) {
    $referencing_entity = (isset($file->referencing_entity)) ? $file->referencing_entity : NULL;

    $id3 = getid3_analyze_file($file);

    $sources = array();
    $sources[] = array(
      'url' => file_create_url($file->uri),
      'mimetype' => $file->filemime,
      'filesize' => $file->filesize,
      'duration' => $id3['playtime_string'],
    );
    // see theme_file_entity_file_audio($variables) for audio rendering
    $element = array(
      '#theme' => 'media_podlove_audio',
      '#options' => array(),
      '#sources' => $sources,
      '#duration' => $sources[0]['duration'],
      '#file' => $file,
      '#referencing_entity' => $referencing_entity,
    );

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    $default_settings_keys = array_keys(_media_podlove_settings_defaults());

    foreach ($default_settings_keys as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }
    return $element;
  }
}

/**
 * Settings callback for podlove_player file formatter
 */
function media_podlove_file_formatter_audio_settings($form, &$form_state, $settings) {
  $element = array();

  $element['title'] = array(
    '#title' => t('Title'),
    '#required' => TRUE,
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['title'],
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
    '#min_tokens' => 1,
  );
  $element['subtitle'] = array(
    '#title' => t('Subtitle'),
    '#required' => TRUE,
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['subtitle'],
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
    '#min_tokens' => 1,
  );
  $element['permalink'] = array(
    '#title' => t('Permalink'),
    '#required' => TRUE,
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['permalink'],
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
    '#min_tokens' => 1,
  );
  $element['summary'] = array(
    '#title' => t('Summary'),
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['summary'],
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
    '#min_tokens' => 1,
  );
  $element['summaryVisible'] = array(
    '#title' => t('Summary visible'),
    '#description' => t(''),
    '#type' => 'checkbox',
    '#default_value' => $settings['summaryVisible'],
    '#return_value' => TRUE,
  );
  $element['chapters'] = array(
    '#title' => t('Chapters'),
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['chapters'],
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
    '#min_tokens' => 1,
  );
  $element['chaptersVisible'] = array(
    '#title' => t('Chapters visible'),
    '#description' => t(''),
    '#type' => 'checkbox',
    '#default_value' => $settings['chaptersVisible'],
    '#return_value' => TRUE,
  );
  $element['poster'] = array(
    '#title' => t('Source of poster image'),
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['poster'],
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#token_types' => array('node', 'file'),
    '#min_tokens' => 0,
  );
  $element['poster_fid'] = array(
    '#title' => t('Default cover image'),
    '#description' => t(''),
    '#type' => 'managed_file',
    '#upload_location' => 'public://files/podlove_cover',
    '#default_value' => $settings['poster_fid'],
  );
  $element['width'] = array(
    '#title' => t('Player width'),
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
    '#required' => TRUE,
  );
  $element['startVolume'] = array(
    '#title' => t('Start volume'),
    '#description' => t(''),
    '#type' => 'textfield',
    '#default_value' => $settings['startVolume'],
  );
  $element['alwaysShowHours'] = array(
    '#title' => t('Always show hours'),
    '#description' => t(''),
    '#type' => 'checkbox',
    '#default_value' => $settings['alwaysShowHours'],
    '#return_value' => TRUE,
  );
  $element['timecontrolsVisible'] = array(
    '#title' => t('Timecontrols visible'),
    '#description' => t(''),
    '#type' => 'checkbox',
    '#default_value' => $settings['timecontrolsVisible'],
    '#return_value' => TRUE,
  );
  $element['sharebuttonsVisible'] = array(
    '#title' => t('Sharebuttons visible'),
    '#description' => t(''),
    '#type' => 'checkbox',
    '#default_value' => $settings['sharebuttonsVisible'],
    '#return_value' => TRUE,
  );
  $element['hidedownloadbutton'] = array(
    '#title' => t('Hide download button'),
    '#description' => t(''),
    '#type' => 'checkbox',
    '#default_value' => $settings['hidedownloadbutton'],
    '#return_value' => TRUE,
  );
  // Display the user documentation of placeholders
  $element['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $element['token_help']['help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node', 'file'),
  );

  return $element;
}

/**
 * Default settings for a newly created option set
 *
 * @param string $key [optional]
 *  Get specific default value
 */
function _media_podlove_settings_defaults($key = NULL) {
  $defaults = array(
    'title' => '[site:name] - Podcast',
    'subtitle' => '[node:title]',
    'summary' => '[node:body:summary]',
    'chapters' => NULL,
    'permalink' => '[node:url]',
    'poster' => NULL,
    'alwaysShowHours' => FALSE,
    'startVolume' => '0.8',
    'width' => 'auto',
    'summaryVisible' => FALSE,
    'timecontrolsVisible' => FALSE,
    'sharebuttonsVisible' => TRUE,
    'chaptersVisible' => FALSE,
    'hidedownloadbutton' => TRUE,
  );

  // Return the specific item
  if (isset($key) and array_key_exists($key, $defaults)) {
    return $defaults[$key];
  }

  // Return all items
  return $defaults;
}

/**
 * Return Settings with tokens
 */
function _media_podlove_settings_with_tokens() {
  return array('title', 'subtitle', 'summary', 'chapters', 'permalink',
    'source_mp4', 'source_ogg', 'source_opus', 'source_poster');
}
