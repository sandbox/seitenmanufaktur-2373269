<?php

/**
 * Form callback for default settings
 */
function media_podlove_admin($form, &$form_state) {
  // preview
  $form['preview'] = array(
    '#title' => t('Preview'),
    '#description' => t('Will be updated after saving the settings.'),
    '#type' => 'markup',
  );
  /*
  $form['media_podlove_poster_fid'] = array(
    '#title' => t('Cover image'),
    '#description' => t(''),
    '#type' => 'managed_file',
    '#upload_location' => 'public://files/podlove_cover',
    '#default_value' => variable_get('media_podlove_poster_fid'),
  );
  */
  $form['media_podlove_width'] = array(
    '#title' => t('Player width'),
    '#description' => t('Playerwidth in Pixel'),
    '#type' => 'textfield',
    '#default_value' => variable_get('media_podlove_width'),
  );
  $form['media_podlove_start_volume'] = array(
    '#title' => t('Start volume'),
    '#description' => t('Start Volume of Player 1=100%, 0.8 = 80%'),
    '#type' => 'textfield',
    '#default_value' => variable_get('media_podlove_start_volume'),
  );
  $form['media_podlove_always_show_hours'] = array(
    '#title' => t('Always show hours'),
    '#description' => t('Show hours as 0'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('media_podlove_always_show_hours'),
  );
  $form['media_podlove_summary_visible'] = array(
    '#title' => t('Summary visible'),
    '#description' => t('Summary visible by default?'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('media_podlove_summary_visible'),
  );
  $form['media_podlove_timecontrols_visible'] = array(
    '#title' => t('Timecontrols visible'),
    '#description' => t('Timecontrols visible by default?'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('media_podlove_timecontrols_visible'),
  );
  $form['media_podlove_sharebuttons_visible'] = array(
    '#title' => t('Sharebuttons visible'),
    '#description' => t('Sharebuttons visible by default?'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('media_podlove_sharebuttons_visible'),
  );
  $form['media_podlove_chapters_visible'] = array(
    '#title' => t('Chapters visible'),
    '#description' => t('Chapters visible by default?'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('media_podlove_chapters_visible'),
  );
  $form['media_podlove_hidedownloadbutton'] = array(
    '#title' => t('Hide download button'),
    '#description' => t('No download options shown.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('media_podlove_hidedownloadbutton'),
  );

  return $form;
}
